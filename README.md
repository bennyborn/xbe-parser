# xbe-title parser #
Opens XBOX 1 iso files, searches for the `default.xbe` and displays the games original title as well as the title id.

## Info
During testing I found out that the parsing may not be perfect for all releases - use with care!
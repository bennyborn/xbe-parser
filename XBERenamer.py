import os
import struct
import string
import re
import tempfile
import subprocess
import sys

scriptPath = os.path.dirname(os.path.realpath(__file__))

def read_unpack(file, format):
    # read and unpack a structure as in struct.unpack
    buffer = file.read(struct.calcsize(format))
    return struct.unpack(format,buffer)

class ExtractError(Exception):
    def __init__(self, message):
        self.message = message
    def __str__(self):
        return str(self.message)


class ReaderError(Exception):
    def __init__(self, message):
        self.message = message
    def __str__(self):
        return str(self.message)


class FileReader:
    patterns = (".iso",)
    archive = False
    def __init__(self, filename):
        try:
            self.file = open(filename,"rb")
        except IOError:
            raise ReaderError(_("Cannot read file:'%s'") % filename)
        self.position = 0
        self.skipped = 0

    def read(self, size):
        self.position += size
        return self.file.read(size)
        
    def skip(self, offset):
        self.position += offset
        self.file.seek(offset, 1)
        self.skipped += offset
        
    def seek(self, position):
        self.skip(position-self.position)
        self.position = position
    
    def close(self):
        self.file.close()


class XisoExtractor:


    def __init__(self):
        self.name = ""
        self.id = ""
        self.region = ""
        self.regions = { 
            "00000001":"NTSC-U",
            "00000007":"NTSC-U",
            "00000002":"NTSC-J",
            "00000003":"NTSC",
            }


    def parse_xbe_xbetool(self,size):

        # seek to beginning of default.xbe
        #self.reader.seek(0)

        # get contents
        xbe = None
        xbe = offset = self.reader.read(size)

        # create temporary xbe file for xbeTool
        tempXBE = None
        tempXBE = open(scriptPath+r"\temp.xbe",mode='w+b')
        tempXBE.write(xbe)
        tempXBE.seek(0)

        # run xbetool
        proc = subprocess.Popen([scriptPath+r"\xbetool.exe", "c", tempXBE.name], stdout=subprocess.PIPE, shell=True)
        (out, err) = proc.communicate()

        tempXBE.close()
        os.remove(tempXBE.name)

        # parse data
        xbeData = None
        xbeData = out.split("\n")

        # get name
        try:
            name = xbeData[4].strip()
            name = name.replace(':',' -')
            self.name = name
        except:
            raise Exception(xbeData)

        # get title id
        id = self.xbe_data_search_field('Title ID',xbeData)[2:10]
        self.id = id

        #get region
        region = self.xbe_data_search_field('Game Region',xbeData)[2:10]

        try:
            self.region = self.regions[ region ]
        except:
            self.region = 'PAL'


    def xbe_data_search_field(self,fieldName,data):

        for line in data:
            if line[:len(fieldName)] == fieldName:
                m = re.search('[\w]+:[\s]+(.*?)$',line)
                return m.group(1)


    def check_file(self,filename,size,sector):

        self.reader.seek(sector*2048)

        if filename.lower() == "default.xbe":

            # parse xbe file
            self.parse_xbe_xbetool(size)


    def browse_entry(self, sector, offset, folders):
        pos = sector*2048+offset
        
        # jump to sector
        self.reader.seek(pos)

        # read file header
        ltable, rtable, newsector, filesize, attributes, filename_size \
        = read_unpack(self.reader, "<HHLLBB")

        # read file name
        filename = self.reader.read(filename_size)
        
        if (attributes & 0x10 > 0) and (filename_size>0):
            nfolders = folders[:]
            nfolders.append(filename)
            self.sector_list.append( (newsector*2048, newsector, 0, "entry", filename, 0, nfolders) )
        else:
            # write file
            if filename:
                self.sector_list.append( (newsector*2048, newsector, 0, "file", filename, filesize, folders) )

        if rtable > 0:
            self.sector_list.append( ( sector*2048+rtable ,sector, rtable*4, "entry", "", 0, folders) )
        if ltable > 0:
            self.sector_list.append( ( sector*2048+ltable ,sector, ltable*4, "entry", "", 0, folders) )


    def browse_sector(self):
        while self.sector_list:
            # maintain list sorted by sector 
            self.sector_list.sort()
        
            pos, sector, offset, type, filename, size, folders = \
                self.sector_list.pop(0)
            if type == "entry":
                self.browse_entry(sector, offset, folders)
            if type == "file":
                self.check_file(filename, size,sector)
                if len(self.name):
                    break;


    def browse_start(self, sector):

        self.sector_list = []
        self.current_folders = []
        self.sector_list.append( (sector*2048, sector, 0, "entry", "", 0, []) )
        self.browse_sector()


    def parse(self,iso_name):

        # make sure file is really readable in whole
        try:
            test = file(iso_name, "rb")
        except IOError:
            raise ExtractError( _("<b>Cannot read iso</b>") )
        
        try:
            test.seek(0,2)
        except IOError:
            raise ExtractError( _("<b>Cannot read whole iso (too big ?)</b>") )
        test.close()

        try:
            self.reader = FileReader(iso_name)
        except ReaderError, error:
            raise ExtractError( _("<b>Cannot open iso:</b>\n%s") % error )

        signature = "\x4d\x49\x43\x52\x4f\x53\x4f\x46\x54\x2a\x58\x42\x4f\x58\x2a\x4d\x45\x44\x49\x41"

        # skip beggining
        self.reader.skip(0x10000)

        # read and verify header
        header = self.reader.read(0x14)
        if header != signature:
            raise ExtractError( ("<b>Not a valid xbox iso image %s</b>" % iso_name) )

        # read root sector address
        root_sector, = read_unpack(self.reader, "<L")

        # skip root_size + FILETIME + unused
        self.reader.skip(0x7d4)

        # read and verify header
        header = self.reader.read(0x14)
        if header != signature:
            raise ExtractError( ("<b>Not a valid xbox iso image</b>") )

        # and start extracting files
        self.browse_start(root_sector)
        self.reader.close()

        return None


def main():

    if not len(sys.argv[1]):
        print "Please set a folder containing game ISOs"
        sys.exit(0)

    os.chdir( sys.argv[1] )

    for iso in os.listdir("."):
        if iso.lower().endswith(".iso"):

            xiso = None
            xiso = XisoExtractor()

            try:
                xiso.parse(iso)
                print 'ren "%s" "%s [%s] [%s].iso"' % (iso,xiso.name,xiso.id,xiso.region)
            except:
                print '%s ERROR PARSING XBE FILE' % (iso)


if __name__ == "__main__":
    main()